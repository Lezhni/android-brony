package by.brony.website;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    private static final String SITE_URL = "http://brony.by";
    private static final int LOCATION_REQUEST = 1;

    private String geoLocationRequestOrigin;
    private GeolocationPermissions.Callback geoLocationCallback;

    WebView mainWebView;
    RelativeLayout progressBarContainer;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.site_progress);
        progressBarContainer = findViewById(R.id.site_progress_container);
        mainWebView = findViewById(R.id.site_webview);

        /* if no internet, show warning message, otherwise load site */
        tryToLoadSite();
    }

    /* use Back button as history back button */
    @Override
    public void onBackPressed() {
        if (mainWebView.canGoBack()) {
            mainWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (LOCATION_REQUEST == requestCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (null != geoLocationCallback) {
                    geoLocationCallback.invoke(geoLocationRequestOrigin, true, true);
                }
            } else {
                if (null != geoLocationCallback) {
                    geoLocationCallback.invoke(geoLocationRequestOrigin, false, false);
                }
            }
        }
    }

    /* expand WebViewClient functionality */
    private class CustomWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            /* if link is mailto */
            if (url.startsWith("mailto:")) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse(url));
                if (null != emailIntent.resolveActivity(getPackageManager())) {
                    startActivity(Intent.createChooser(emailIntent, "Приложение для отправки письма"));
                }
                return true;
            }

            view.loadUrl(url);
            return true;
        }
    }

    /* expand WebChromeClient functionality */
    private class CustomWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int progress) {
            progressBar.setProgress(progress);
            if (100 == progress) {
                progressBar.setVisibility(View.GONE);
                progressBarContainer.setVisibility(View.GONE);
            } else {
                progressBar.setVisibility(View.VISIBLE);
                progressBarContainer.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            geoLocationRequestOrigin = null;
            geoLocationCallback = null;

            /* request permission to access geolocation, if needed */
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                geoLocationRequestOrigin = origin;
                geoLocationCallback = callback;
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            } else {
                /* tell the WebView that permission has been granted */
                callback.invoke(origin, true, true);
            }
        }
    }

    /* prepare WebView and load an URL */
    @SuppressLint("SetJavaScriptEnabled")
    private void tryToLoadSite() {
        if (!isConnectionExist()) {
            final Snackbar internetWarning = Snackbar.make(findViewById(R.id.parent_layout), "Интернет отсутствует.", Snackbar.LENGTH_INDEFINITE);
            internetWarning.setAction("Повторить", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    internetWarning.dismiss();
                    tryToLoadSite();
                }
            });
            internetWarning.show();
        } else {
            mainWebView.setWebViewClient(new CustomWebViewClient());
            mainWebView.setWebChromeClient(new CustomWebChromeClient());
            mainWebView.getSettings().setJavaScriptEnabled(true);
            mainWebView.getSettings().setGeolocationEnabled(true);
            mainWebView.loadUrl(SITE_URL);
        }
    }

    /* check is no internet connection */
    private boolean isConnectionExist() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
